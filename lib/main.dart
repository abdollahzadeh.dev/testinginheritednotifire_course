import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class SliderData extends ChangeNotifier {
  double _value =0.0;
  double get value => _value;
  set value(double newValue){
    if(newValue != _value){
      _value =newValue;
      notifyListeners();
    }
  }

  }

final sliderData =SliderData();
class SliderInheritedNotifier extends InheritedNotifier<SliderData> {
  const SliderInheritedNotifier({Key? key ,
    required SliderData sliderData,
    required Widget child}) :
        super(key: key ,
          notifier: sliderData,
          child: child);
  static double of(BuildContext context){
   return context.
    dependOnInheritedWidgetOfExactType<SliderInheritedNotifier>()
        ?.notifier
        ?.value ??
    0.0;
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('inheritedNotifier'),
      ),
      body: SliderInheritedNotifier(
        sliderData: sliderData,
        child: Builder(
          builder: (context){
            return Column(
              children: [
                Slider(
                    value: SliderInheritedNotifier.of(context),
                    onChanged: (value){
                      sliderData.value = value;
                    }),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Opacity(
                      opacity: SliderInheritedNotifier.of(context),
                      child: Container(
                        height: 200,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [Colors.red , Colors.black])
                        ),
                      ),
                    ),
                    Opacity(
                      opacity: SliderInheritedNotifier.of(context),
                      child: Container(
                        height: 200,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [Colors.blue , Colors.green])
                        ),
                      ),
                    )
                  ].expandEqually().toList(),)
              ],
            );
          },

        ),
      ),
    );
  }
}
extension ExpandEqually on Iterable<Widget>{
  Iterable<Widget> expandEqually() => map((w) => Expanded(child : w));
}
